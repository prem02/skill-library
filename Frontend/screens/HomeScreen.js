import React, {useState} from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  TouchableNativeFeedback,
} from 'react-native';
import {Colors, Fonts, H1, H2, Normalize} from '../Constants/Theme';
import {Skillsearchcont2, Skillinp, Skillcont} from '../styles/SkillSelect';
// import { Container, SearchBoxCont } from '../styles/SearchBox';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';

import {AppTitle} from '../styles/Login';
import SearchBox from '../components/SearchBox';
import {windowWidth} from '../utils/Dimension';
import {APIURI} from '../Constants/APIconfigs';
import {useDispatch, useSelector} from 'react-redux';
import axios from 'axios';
import {setSearchResult} from '../Redux/actions';

const HomeScreen = ({navigation}) => {
  const [text, setText] = useState('');

  let data = useSelector(state => state.mainReducer);

  const dispatch = useDispatch();
  const textHandler = val => {
    setText(val);
  };

  const textClearHandler = () => {
    setText('');
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <View
        style={{
          justifyContent: 'space-around',
          alignItems: 'center',
          flexDirection: 'row',
          marginTop: 22,
        }}>
        <View
          style={{
            // justifyContent: 'space-around',
            // alignItems: 'center',
            flexDirection: 'row',
          }}>
          <AntDesign name="enviromento" size={20} color={Colors.primary} />

          <H2
            style={{
              color: 'black',
              marginLeft: 15,
              fontSize: 14,
              fontFamily: Fonts.PoppinsMedium,
              // marginTop: -20,
            }}>
            Chennai Central,Chennai
          </H2>
        </View>
        <Image
          source={require('../assets/images/logo.png')}
          style={{height: 50, width: 50}}
        />
      </View>
      <View
        style={{
          flex: 1,
          justifyContent: 'space-evenly',
          alignItems: 'center',
        }}>
        <AppTitle style={{fontSize: 38}}>Skill Library</AppTitle>
        <View style={{alignItems: 'flex-start'}}>
          <H1
            style={{
              textAlign: 'center',
              color: Colors.greyDark,
              fontFamily: Fonts.PoppinsBold,
              fontSize: Normalize(20),
              marginLeft: 22,

              // marginRight: 150,
              // marginTop: 181,
            }}>
            Search a Skill
          </H1>
          {/* <SearchBox  /> */}

          <View style={{paddingTop: 15}}>
            <Skillcont>
              <Skillsearchcont2 style={{elevation: 3}}>
                <AntDesign
                  style={{marginTop: 12, marginLeft: 26}}
                  name="search1"
                  size={20}
                  color={Colors.primary}
                />
                <Skillinp
                  value={text}
                  placeholder="Try logo Designer"
                  placeholderTextColor={Colors.greyplaceholder}
                  onChangeText={textHandler}
                />
                <TouchableOpacity onPress={textClearHandler}>
                  <Entypo
                    name="chevron-thin-right"
                    size={20}
                    color={Colors.black}
                    style={{marginTop: 12, marginLeft: 11}}
                  />
                  <Entypo
                    name="chevron-thin-left"
                    size={20}
                    color={Colors.black}
                    style={{marginTop: -20.5, marginLeft: 20}}
                  />
                </TouchableOpacity>
              </Skillsearchcont2>
            </Skillcont>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 15,
              }}>
              <TouchableNativeFeedback
                onPress={async () => {
                  await axios
                    .post(`${APIURI}search`, {
                      Id: data.oldUser.user.id,
                      skill: text,
                      long: 80.21660614,
                      lat: 13.07819653,
                    })
                    .then(res => {
                      dispatch(setSearchResult(res.data));
                    })
                    .catch(e => console.log(e));

                  navigation.navigate('SearchResult', {
                    text: text,
                  });
                  console.log(text);
                }}>
                <View
                  style={{
                    backgroundColor: Colors.primary,
                    padding: 13,
                    borderRadius: 15,
                    width: windowWidth * 0.3,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      color: Colors.white,
                      fontFamily: Fonts.PoppinsMedium,
                    }}>
                    Search
                  </Text>
                </View>
              </TouchableNativeFeedback>
            </View>
          </View>
        </View>

        <Image source={require('../assets/images/homepage.png')} />
      </View>
    </View>
  );
};
export default HomeScreen;
