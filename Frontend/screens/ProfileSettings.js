import React from "react";
import {View,Text,StyleSheet,TouchableOpacity,TextInput,ScrollView,StatusBar,Dimensions} from 'react-native';
import {Colors,Fonts,H1,H2,Normalize,ButtonFont3} from '../Constants/Theme';
import {Skillsearchcont2,Skillinp,Skillcont,} from '../styles/SkillSelect';
import FormbackButton from '../components/buttons/FormbackButton';
import AntDesign from 'react-native-vector-icons/AntDesign';

const{height} = Dimensions.get('window') 

const ProfileSettings = ({ navigation}) => {
  state = {
     screenHeight:0,
  };
  onContentSizeChange = (contentWidth,contentHeight) => {
    this.setState({screenHeight:contentHeight}) ;
  };
  const scrollEnabled= this.state.screenHeight > height;
    return(
        <View style={{ backgroundColor: Colors.white,flex:1}}>
          <View style={{flex:0.23,top:-27,borderRadius:25,backgroundColor: '#FFFFFF',elevation:5}}></View>
            <View style={{position: 'absolute',alignSelf: 'flex-start',marginLeft:23,marginTop:45}}>
        <TouchableOpacity onPress={() => navigation.navigate('HomeScreen')} >
          <FormbackButton />
        </TouchableOpacity>
         <H1 style={{color: 'black',marginLeft:65,marginTop:20,fontSize:18,fontFamily: Fonts.PoppinsMedium,}}>ProfileSettings</H1>
         <View style={{elevation: 3,width: 89,height:30,justifyContent: 'center',alignItems: 'center',alignSelf: 'flex-start',backgroundColor: Colors.primary,borderRadius: 20,marginLeft:210,marginTop:-28}}>
            <ButtonFont3 style={{color: Colors.white}}>Save</ButtonFont3>
          </View> 
          
           
            <ScrollView 
               scrollEnabled={scrollEnabled} 
               onContentSizeChange = {this.onContentSizeChange }
               >
           <H2 style={{color: 'black',marginLeft:25,marginTop:90,fontSize:18,fontFamily: Fonts.PoppinsMedium,}}>Name</H2>
           <TextInput style={{ backgroundColor: Colors.white,elevation: 3,borderRadius: 17,paddingLeft: 25,marginLeft:15,color: Colors.black,fontFamily: Fonts.PoppinsMedium,padding: 10,fontSize: Normalize(18),}}
        placeholder={"Karthik"}
        placeholderTextColor={'#000000'}> </TextInput>
        <H2 style={{color: 'black',marginLeft:25,marginTop:15,fontSize:18,fontFamily: Fonts.PoppinsMedium,}}>Username</H2>     
        <TextInput style={{ backgroundColor: Colors.white,elevation: 3,borderRadius: 17,marginTop:15,paddingLeft: 25,marginLeft:15,color: Colors.black,fontFamily: Fonts.PoppinsMedium,padding: 10,fontSize: Normalize(18),}}> </TextInput>   
        <H2 style={{color: 'black',marginLeft:25,marginTop:15,fontSize:18,fontFamily: Fonts.PoppinsMedium,}}>Tagline</H2>    
        <TextInput style={{ backgroundColor: Colors.white,elevation: 3,borderRadius: 17,marginTop:15,paddingLeft: 25,marginLeft:15,color: Colors.black,fontFamily: Fonts.PoppinsMedium,padding: 10,fontSize: Normalize(18),}}> </TextInput>
        <H2 style={{color: 'black',marginLeft:25,marginTop:15,fontSize:18,fontFamily: Fonts.PoppinsMedium,}}>Date of Birth</H2>   
        <TextInput style={{ backgroundColor: Colors.white,elevation: 3,borderRadius: 17,marginTop:15,paddingLeft: 25,marginLeft:15,color: Colors.black,fontFamily: Fonts.PoppinsMedium,padding: 10,fontSize: Normalize(18),}}> <AntDesign
              style={{}}
                name="calendar"
                size={20}
                color="#00A8E8" /> </TextInput>
        <H2 style={{color: 'black',marginLeft:25,marginTop:15,fontSize:18,fontFamily: Fonts.PoppinsMedium,}}>Gender</H2>
        <TextInput style={{ backgroundColor: Colors.white,elevation: 3,borderRadius: 17,marginTop:15,paddingLeft: 25,marginLeft:15,color: Colors.black,fontFamily: Fonts.PoppinsMedium,padding: 10,fontSize: Normalize(18),}}> 
         </TextInput><AntDesign
              style={{paddingLeft:260,bottom:32}}
                name="down"
                size={20}
                color='black' />
           <H2 style={{color: 'black',marginLeft:25,marginTop:15,fontSize:18,fontFamily: Fonts.PoppinsMedium,}}>Description</H2>
           <TextInput style={{ backgroundColor: Colors.white,elevation: 3,borderRadius: 17,marginTop:15,paddingLeft: 25,marginLeft:15,color: Colors.black,fontFamily: Fonts.PoppinsMedium,padding: 10,fontSize: Normalize(18),}}> 
         </TextInput>
         <H2 style={{color: 'black',marginLeft:25,marginTop:15,fontSize:18,fontFamily: Fonts.PoppinsMedium,}}>Role</H2>
       </ScrollView>
      
      </View>
       </View>



);
};
export default ProfileSettings ;

/*      <View style={{backgroundColor: '#FFFFFF',elevation:5,height: 180,width:365,top: -27,borderRadius:25}}>
         
                    <H1 style={{ color: 'black',marginTop: 95,marginLeft:105,fontSize:18, fontFamily: Fonts.PoppinsMedium,}}> Profile Settings </H1> 
                    <TouchableNativeFeedback onPress={() => route.params.onsubmit(value)}>
          <View
            style={{
              elevation: 3,
              width: 89,
              height:30,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: Colors.primary,
              borderRadius: 20,
            }}>
            <ButtonFont3 style={{color: Colors.white}}>Save</ButtonFont3>
          </View>
        </TouchableNativeFeedback>
                    <View style={{alignItems: 'center', marginTop: 80}}>
            
      </View>

            </View> */