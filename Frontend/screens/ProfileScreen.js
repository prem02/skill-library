import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  FlatList,
} from 'react-native';
import React from 'react';
import {
  AddBtn,
  AddBtnCont,
  AddTxt,
  Container,
  H1,
  IconCont,
  InfoCont,
  LevelBtn,
  LevelCont,
  LevelTxt,
  Name,
  Optionsbtn,
  OptionsCont,
  OptionsTxt,
  Phone,
  ProfileCont,
  ProfilePic,
  ReferCont,
  Skill,
} from '../styles/Profile';
import {useDispatch, useSelector} from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import OctIcons from 'react-native-vector-icons/Octicons';
import {windowHeight, windowWidth} from '../utils/Dimension';
import {Colors, H2} from '../Constants/Theme';
import auth from '@react-native-firebase/auth';
import {setOldUser} from '../Redux/actions';
// window width, height
export const Window = {
  width: windowWidth,
  height: windowHeight,
};

const {width, height} = Window;

const details = [
  {
    detail: 'Add Email',
    id: 1,
  },
  {
    detail: 'Skill Description',
    id: 2,
  },
  {
    detail: 'Add Your Gender',
    id: 3,
  },
  {
    detail: 'Add Your DOB',
    id: 4,
  },
  {
    detail: 'Set Cool User Name',
    id: 5,
  },
];

const ProfileScreen = () => {
  // let data = useSelector(state => state.mainReducer.name);
  const dispatch = useDispatch();
  const stateData = useSelector(state => state.mainReducer);

  // retrieving name from state & storing in the userName var
  const userName = stateData.oldUser ? stateData.oldUser.user.name : '';

  // retrieving mobilenum from state & storing in the userPhone var
  const userPhone = stateData.oldUser ? stateData.oldUser.user.phone : '';

  // retrieving tagline from state & storing in the userTagline var
  const userTagline = stateData.oldUser ? stateData.oldUser.user.tagline : '';

  const handleLogOut = () => {
    dispatch(setOldUser(false));
    auth().signOut();
  };
  return (
    <Container>
      <InfoCont>
        <ProfilePic source={require('../assets/images/profile.jpg')} />
        <IconCont>
          <TouchableOpacity>
            <Ionicons
              name="share-social-outline"
              color={'#000'}
              size={25}
              style={styles.share}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <AntDesign name="edit" color={'#000'} size={25} />
          </TouchableOpacity>
        </IconCont>
        <ProfileCont>
          <Name>{userName}</Name>
          <Skill>{userTagline}</Skill>
          <Phone>{userPhone}</Phone>
        </ProfileCont>
      </InfoCont>
      <ScrollView style={{marginBottom: windowHeight * 0.1}}>
        <H1>40% of Profile Completeness</H1>
        <H2 style={styles.h2}>Add details to complete now</H2>
        <FlatList
          style={{height: windowHeight * 0.07}}
          showsHorizontalScrollIndicator={false}
          horizontal
          data={details} // set render data in flatlist
          keyExtractor={item => item.id.toString()} // keyExtractor convert INT  'item.id' value to string
          numColumns={1}
          renderItem={({item}) => (
            <TouchableOpacity style={{backgroundColor: 'white'}}>
              <AddTxt>{item.detail}</AddTxt>
            </TouchableOpacity>
          )}
        />
        {/* <LevelCont>
          <LevelBtn>
            <LevelTxt>Level 1</LevelTxt>
          </LevelBtn>
          <ReferCont>
            <H1
              style={{
                marginLeft: windowWidth * 0.13,
              }}>
              2 Referral
            </H1>
            <H2 style={{color: 'black'}}>You Refered 2 friends</H2>
          </ReferCont>
        </LevelCont> */}
        <Optionsbtn style={styles.option}>
          <OptionsCont>
            <Ionicons
              name="settings-outline"
              color={Colors.primary}
              size={30}
            />
            <OptionsTxt>Profile Settings</OptionsTxt>
          </OptionsCont>
          <TouchableOpacity
            hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}>
            <FontAwesome5 name="play" color={Colors.arrow} size={25} />
          </TouchableOpacity>
        </Optionsbtn>
        <Optionsbtn style={styles.option}>
          <OptionsCont>
            <MaterialIcons name="security" color={Colors.primary} size={30} />
            <OptionsTxt>Security Settings</OptionsTxt>
          </OptionsCont>
          <TouchableOpacity
            hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}>
            <FontAwesome5 name="play" color={Colors.arrow} size={25} />
          </TouchableOpacity>
        </Optionsbtn>
        <Optionsbtn style={styles.option}>
          <OptionsCont>
            <Ionicons name="heart-outline" color={Colors.primary} size={30} />
            <OptionsTxt>Referral & MMS ID</OptionsTxt>
          </OptionsCont>
          <TouchableOpacity
            hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}>
            <FontAwesome5 name="play" color={Colors.arrow} size={25} />
          </TouchableOpacity>
        </Optionsbtn>
        <Optionsbtn style={styles.option}>
          <OptionsCont>
            <OctIcons name="file-badge" color={Colors.primary} size={30} />
            <OptionsTxt>Badges</OptionsTxt>
          </OptionsCont>
          <TouchableOpacity
            hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}>
            <FontAwesome5 name="play" color={Colors.arrow} size={25} />
          </TouchableOpacity>
        </Optionsbtn>
        <Optionsbtn style={styles.option}>
          <OptionsCont>
            <MaterialIcons
              name="privacy-tip"
              color={Colors.primary}
              size={30}
            />
            <OptionsTxt>Privacy Policy</OptionsTxt>
          </OptionsCont>
          <TouchableOpacity
            hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}>
            <FontAwesome5 name="play" color={Colors.arrow} size={25} />
          </TouchableOpacity>
        </Optionsbtn>

        <TouchableOpacity
          onPress={() => handleLogOut()}
          style={{
            elevation: 5,
            backgroundColor: Colors.white,
            width: windowWidth * 0.4,
            borderRadius: 20,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
            padding: 8,
            marginVertical: 10,
            marginLeft: 30,
          }}>
          <MaterialIcons name="logout" color={'red'} size={30} />
          <OptionsTxt style={{color: 'red'}}>Logout</OptionsTxt>
        </TouchableOpacity>
      </ScrollView>
    </Container>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({
  share: {
    paddingBottom: windowHeight * 0.05,
  },

  h2: {
    marginLeft: windowWidth * 0.08,
    color: Colors.black,
  },
  option: {
    borderTopColor: '#fff',
  },
});
