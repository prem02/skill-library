import {View, Text, Easing} from 'react-native';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {
  createNativeStackNavigator,
  headerStyleInterpolators,
  TransitionSpecs,
  TransitionPresets,
  CardStyleInterpolators,
} from '@react-navigation/native-stack';
// import SkillScreen from '../screens/SkillScreen';
import NamesScreen from '../screens/NamesScreen';
import OTPScreen from '../screens/LoginScreens/OTPScreen';
import LocationsScreen from '../screens/LocationsScreen';
import Dummy from '../screens/Dummy';
// import Skill from '../screens/SkillScreen';
import HomeScreen from '../screens/HomeScreen';
import Somma from '../screens/Somma';
import ProfileScreen from '../screens/ProfileScreen';
import Home from './Home';
import UserProfileScreen from '../components/UserProfileScreen';
import SearchScreen from '../screens/SearchScreen';
// import SkillsScreen from '../screens/SKillsScreen';
import {SkillSelect} from '../styles/SkillSelect';
import SearchSkill from '../screens/SearchSkill';
import {SkillScreen} from '../screens';
import SelectSkill from '../screens/SelectSkill';

// import {LocationsScreen, SkillScreen, NamesScreen} from '../screens/';
const Stack = createNativeStackNavigator();

const Registration = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        headerMode="none"
        initialRouteName="Skill"
        screenOptions={{
          gestureEnabled: true,
          gestureDirection: 'vertical',
        }}>
        <Stack.Screen
          name="Skill"
          component={SkillScreen}
          options={{
            header: () => null,
            animation: 'slide_from_right',
          }}
        />
        <Stack.Screen
          name="Location"
          component={LocationsScreen}
          options={{header: () => null, animation: 'slide_from_right'}}
        />
        <Stack.Screen
          name="Names"
          component={NamesScreen}
          options={{header: () => null, animation: 'slide_from_right'}}
        />
        <Stack.Screen
          name="Somma"
          component={SearchScreen}
          options={{header: () => null}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Registration;
