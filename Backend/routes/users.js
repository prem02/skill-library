var express = require("express");
const jwt = require("jsonwebtoken");
const auth = require("../middleware/auth");
var router = express.Router();
const { ensureAuth, ensureGuest } = require("../middleware/gAuth");
const { sequelize } = require("../models");
const db = require("../models");
const user = require("../models/user");
const multer = require("multer");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./uploads/");
  },
  filename: function (req, file, cb) {
    cb(null, `${Date.now()}-skilllib-${file.originalname}`);
  },
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5,
  },
  fileFilter: fileFilter,
});

const User = db.user;
const UserSkill = db.userskill;
const op = db.Sequelize.Op;

// REMOVED AUTH FOR TESTING _ ADD LATER ***************

// jwt token generator

router.post("/login", async (req, res) => {
  // Our login logic starts here
  try {
    // Get user input
    // console.log(req.body.phone);
    const phone = req.body.phone;
    console.log(phone, "PHONE");
    // Validate user input
    // if (!(email && password)) {
    //   res.status(400).send("All input is required");
    // }
    // Validate if user exist in our database
    const user = await User.findOne({ where: { phone: phone } });

    if (user) {
      // Create tokens
      const token = jwt.sign({ user_id: user.id }, process.env.TOKEN_KEY, {
        expiresIn: "48h",
      });

      // user
      res.status(200).json({ user: user, token: token });
    } else {
      res.status(200).json(false);
    }
  } catch (err) {
    console.log(err);
  }
  // Our register logic ends here
});

// ...

// route for image uploadation
router.put(
  "/profile/:id",
  upload.single("profile_photo"),
  async function (req, res, next) {
    console.log(req.file);
    const id = req.params.id;
    console.log(req.body);
    const user = await User.update(
      { profile_photo: req.file.path },
      {
        where: { id: id },
      }
    ).catch((err) => {
      console.log(err);
    });
    res.json(user);
  }
);

// search route
router.get("/search", async function (req, res, next) {
  const Id = 1;
  const skill = "developer";
  const searchresults = await sequelize.query(`SELECT *,( 6371 * 
    acos( cos( radians(13.19894200) )* 
    cos( radians( latitude ) ) * 
    cos( radians( longitude ) - 
    radians(80.16575200) ) + 
    sin( radians(13.19894200) ) * 
    sin( radians( latitude ) ) ) ) AS distance 
    FROM ahbjobs.users
    where (users.skills = "${skill}" and users.id != ${Id})
    HAVING (distance < 15)
    ORDER BY distance LIMIT 0 , 20;`);
  res.json(searchresults);

  console.log("usertest");
});
/* GET users listing. */

// use ** auth for protection
router.get("/:id", async function (req, res, next) {
  const id = req.params.id;
  const user = await User.findByPk(id).catch((err) => {
    console.log(err);
  });
  res.json(user);
  console.log("user", user);
});

// New user route @post
router.post("/registration", async function (req, res, next) {
  const newUser = req.body;
  console.log("here", req.body);
  // const newUser = JSON.parse(req.body);
  let skillArr = req.body.skillList;
  console.log(typeof skillArr);

  // let skillArr = '[1,2,3]'
  skillArr = skillArr.substring(1);
  skillArr = skillArr.substring(0, skillArr.length - 1);

  const skillArrList = skillArr.split(",");

  const newUserDB = await User.create(newUser).catch((err) => {
    console.log(err);
  });

  await skillArrList.forEach(async (skill) => {
    console.log(newUserDB.id, skill);

    await sequelize
      .query(
        `INSERT INTO ahbjobs.userskills (createdAt,updatedAt,userId, skillId) VALUES (NOW(),NOW(),${newUserDB.id}, ${skill});`
      )
      .catch((err) => console.log(err));
  });

  const token = jwt.sign({ user_id: newUserDB.id }, process.env.TOKEN_KEY, {
    expiresIn: "48h",
  });

  // user
  res.status(200).json([newUserDB, { token: token }]);
});

//user details update route @put request

router.post("/test", async (req, res) => {
  await sequelize.query(
    `INSERT INTO ahbjobs.userskills (createdAt,updatedAt,userId, skillId) VALUES (NOW(),NOW(),'14', '3');`
  );
  res.send("ok");
});

router.put("/:id", async function (req, res, next) {
  const id = req.params.id;
  console.log(req.body);
  const user = await User.update(req.body, {
    where: { id: id },
  }).catch((err) => {
    console.log(err);
  });
  res.json(user);
});

// route to delete a user using ID - @Delete /
router.delete("/:id", async (req, res, next) => {
  const id = req.params.id;
  console.log(req.body);
  const deleteUser = await User.destroy({ where: { id: id } }).catch((err) => {
    console.log(err);
  });
  res.json(deleteUser);
});

// router.get("/search", async (req, res,next) => {
// console.log("dfsdsdhake")
// console.log(req.body);
// const userId = 1;
// const skill = "developer"
// const searchResult = await User.findAll({
//   attributes: [
//     userId
//   ]

// })
// console.log("kedkadl",searchResult)
// });
module.exports = router;
