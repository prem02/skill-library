var express = require("express");
var router = express.Router();
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const verify = require("../controllers/GoogleLoginRN");

const { NotFound } = require("http-errors");
const { body, validationResult } = require("express-validator");

const { ensureAuth, ensureGuest } = require("../middleware/gAuth");

const db = require("../models/index");
const user = require("../models/user");
const User = db.user;
const Inbox = db.inbox;

const { sequelize } = require("../models/index");
const { query } = require("express");
const Skill = db.skills;

// skills list routes
router.get("/skills", async function (req, res, next) {
  const skillslist = await Skill.findAll({
    order: Sequelize.literal("rand()"),
    limit: 10,
  });
  console.log(skillslist);
  res.json(skillslist);
});

// nearby jobs location search route
router.post("/search", async function (req, res, next) {
  const { Id, skill, long, lat } = req.body;

  console.log(Id);
  console.log(skill);
  console.log(long);
  console.log(lat);
  const skilltable = await Skill.findOne({
    where: {
      skills: {
        [Op.like]: "%" + skill + "%",
      },
    },
  });

  try {
    const skillid = skilltable.id;
    console.log(skillid);

    const searchresults =
      await sequelize.query(`SELECT userskills.userId, users.name, users.Tagline,users.Work_Description,users.profile_photo, ( 6371 * 
    acos( cos( radians(${lat}) )* 
    cos( radians( latitude ) ) * 
    cos( radians( longitude ) - 
    radians(${long}) ) + 
    sin( radians(${lat}) ) * 
    sin( radians( latitude ) ) ) ) AS distance FROM users
INNER JOIN userskills
ON (users.id = userskills.userId and userskills.skillId = ${skillid})
where (users.id != ${Id})
    HAVING (distance < 15)
    ORDER BY distance LIMIT 0 , 20;`);
    res.json(searchresults);

    console.log("usertest");
  } catch (error) {
    res.json({});
  }
});

/* GET home route. */
router.get("/", ensureGuest, function (req, res, next) {
  res.json({ "Skill library": "Connected" });
});

//To verify google logins by TokenID
router.post("/google/verify", (req, res) => {
  const Token = req.body.idToken;
  verify(Token)
    .then((result) => {
      console.log(result.jwtToken);
      res.json({ JWTToken: result.jwtToken });
    })
    .catch((err) => {
      console.log(err);
      res.send("failed");
    });
});

router.get("/logout", (req, res) => {
  req.logOut();
  res.redirect("/");
});

//skill autocomplete query route
router.get("/skillquery/:sq", async (req, res) => {
  const query = req.params.sq;
  const skillList = await Skill.findAll({
    limit: 3,
    where: {
      skills: {
        [Op.like]: query + "%",
      },
    },
  });
  // const skillsArr = [];
  // skillList.forEach((crrVal) => {
  //   skillsArr.push(crrVal.skills);
  // });

  res.json({ skillsArr: skillList });
});

router.get("/inboxID/:sender/:receiver", async (req, res) => {
  const { sender, receiver } = req.params;
  const chkInbox = await Inbox.findOne({
    where: {
      [Op.or]: [
        {
          [Op.and]: [{ sender: sender }, { receiver: receiver }],
        },
        {
          [Op.and]: [{ receiver: sender }, { sender: receiver }],
        },
      ],
    },
  }).catch((err) => console.log(err));

  if (chkInbox) {
    res.json({ Inboxid: chkInbox.dataValues.id });
  } else {
    res.json({ Inboxid: 0 });
  }
});

module.exports = router;
