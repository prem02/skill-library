var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
bodyParser = require("body-parser");
// var chatRoute = require("./routes/chat");
var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var adminRouter = require("./routes/admin.router");
const passport = require("passport");
const session = require("express-session");
// require("./controllers/googleLogin")(passport);
var authRoute = require("./routes/googleAuth");
// const user = require("./models/user");

var app = express();

// const db = require("../models");
// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "hbs");

app.use("/uploads", express.static("uploads"));
app.use(logger("dev"));
app.use(express.json());

app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

// creating 24 hours from milliseconds
const oneDay = 1000 * 60 * 60 * 24;

//session middleware
app.use(
  session({
    secret: "keyboard cat",
    cookie: { maxAge: oneDay },
    resave: false,
    saveUninitialized: true,
  })
);
app.use("/admin", adminRouter);

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());
// var http = require("http");
// var server = http.createServer(app);

// /**
//  * Listen on provided port, on all network interfaces.
//  */

// server.listen(process.env.port);
// // server.on("error", onError);
// // server.on("listening", onListening);
// console.log("SERVER STARTED ON PORT");

//db ORM
const db = require("./models");
const { Op } = require("sequelize");

db.sequelize.sync({ force: false }).then(() => {
  console.log("Drop and re-sync db.");
});

// db.sequelize.sync();
const user = db.user;
const Inbox = db.inbox;
const Message = db.message;
app.use("/", indexRouter);
app.use("/user", usersRouter);
app.use("/auth", authRoute);

// app.use("/chat", chatRoute);

// const server = require("http").createServer().listen(3001);

// const { WebSocketServer } = require("ws");

// const { Server } = require("socket.io");
// const io = new Server(server);
// // const server = new WebSocketServer({ port: 3001 });

// io.on("connection", (socket) => {
//   // send a message to the client
//   console.log("You are connected to Socket");
//   socket.send(
//     JSON.stringify({
//       type: "hello from server",
//     })
//   );

//   // receive a message from the client
//   // socket.on("message", (data) => {
//   //   const packet = JSON.parse(data);
//   //   console.log(packet);
//   //   switch (packet.type) {
//   //     case "hello from client":
//   //       console.log(packet);
//   //       break;
//   //   }
//   // });

//   // socket.on("newUser", async (credentials) => {
//   //   const phone = credentials;
//   //   const users = await user.findByPk(1);
//   //   // mobileSockets[user[0].id] = socket.id;
//   //   socket.emit("UserFound", { users });
//   //   // socket.broadcast.emit("newUser", user[0]);
//   // });

//   socket.on("message", async (data) => {
//     const values = JSON.parse(data);
//     // const { text, sender, receiver } = values;

//     console.log(values);
//     const inboxupdate = await Inbox.create(values).catch((err) =>
//       console.log(err)
//     );
//     // const messageupdate = await Message.create(values).catch((err) =>
//     //   console.log(err)
//     // );
//     socket.emit("inbox", { inboxupdate });
//   });

//   socket.on("inboxParticipants", async (data) => {
//     const values = JSON.parse(data);
//     // const { text, sender, receiver } = values;

//     const chkInbox = await Inbox.findAll({
//       where: {
//         [Op.or]: [{ sender: values.id }, { receiver: values.id }],
//       },
//       include: user,
//     }).catch((err) => console.log(err));

//     if (chkInbox) {
//       socket.emit("inboxParticipants", { chkInbox });
//     } else {
//       socket.emit("inboxParticipants", {});
//     }
//   });

//   socket.on("newMessage", async (data) => {
//     const values = JSON.parse(data);
//     // const { text, sender, receiver } = values;

//     const chkInbox = await Inbox.findOne({
//       where: {
//         [Op.or]: [
//           {
//             [Op.and]: [
//               { sender: values.sender },
//               { receiver: values.receiver },
//             ],
//           },
//           {
//             [Op.and]: [
//               { receiver: values.sender },
//               { sender: values.receiver },
//             ],
//           },
//         ],
//       },
//     }).catch((err) => console.log(err));

//     // console.log("HERE    ", chkInbox.dataValues.id);

//     if (chkInbox) {
//       const inboxupdate = await Inbox.update(values, {
//         where: {
//           id: chkInbox.dataValues.id,
//         },
//       }).catch((err) => console.log(err));
//       const messageValues = {
//         sender: values.sender,
//         receiver: values.receiver,
//         text: values.last_msg,
//         inbox_id: chkInbox.dataValues.id,
//       };
//       const messageupdate = await Message.create(messageValues).catch((err) =>
//         console.log(err)
//       );
//       const AllMessage = await Message.findAll({
//         where: {
//           inbox_id: chkInbox.dataValues.id,
//         },
//       }).catch((err) => console.log(err));

//       socket.emit("inbox", { AllMessage, inboxupdate, messageupdate });
//     } else {
//       const inboxupdate = await Inbox.create(values).catch((err) =>
//         console.log(err)
//       );

//       const messageValues = {
//         sender: values.sender,
//         receiver: values.receiver,
//         text: values.last_msg,
//         inbox_id: inboxupdate.dataValues.id,
//       };
//       const messageupdate = await Message.create(messageValues).catch((err) =>
//         console.log(err)
//       );
//       const AllMessage = await Message.findAll({
//         where: {
//           inbox_id: inboxupdate.dataValues.id,
//         },
//       }).catch((err) => console.log(err));

//       socket.emit("inbox", { inboxupdate, messageupdate, AllMessage });
//     }

//     // const inboxupdate = await Inbox.create(values).catch((err) =>
//     //   console.log(err)
//     // );
//     // console.log("inbox", inboxupdate.dataValues.id);
//     // const messageValues = {
//     //   sender: inboxupdate.dataValues.sender,
//     //   receiver: inboxupdate.dataValues.receiver,
//     //   text: inboxupdate.last_msg,
//     //   inbox_id: inboxupdate.dataValues.id,
//     // };
//     // const messageupdate = await Message.create(messageValues).catch((err) =>
//     //   console.log(err)
//     // );
//     // socket.emit("inbox", { inboxupdate, messageupdate });
//   });
// });

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
